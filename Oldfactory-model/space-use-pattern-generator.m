%%
close all;

%% SUPER IMPORTANT - READ THIS!!
% You need x_result(i), y_result(i) computed BEFORE running this!!
% x_result(i): x location of i-th step (float)
% y_result(i): y location of i-th step (float)

%% Constants for plotting. 

% Resize this for better view
plot_radius = 100;

%% Logic for obtaining the space use pattern

% setting up the map
plot_size = 2 * plot_radius + 1;
usePattern = zeros(plot_size, plot_size);
for i = 1:x_limit
    x_location = round(x_result(i));
    y_location = round(y_result(i));
    
    if (abs(x_location) > plot_radius || abs(y_location) > plot_radius)
        % can't draw, just skip this data
        fprintf("Warning! Data is outside of plotting boundary.\n");
        fprintf("\tStep: %d, Location: (%d, %d)\n", i, x_result(i), y_result(i));
        continue;
    end
    
    usePattern(x_location + plot_radius + 1, y_location + plot_radius + 1) ...
        = usePattern(x_location + plot_radius + 1, y_location + plot_radius + 1) + 1;
end

%% Plotting

% Draw this plot on figure 2. 
% Change this if you want to plot it elsewhere!
figure(2);

% Surf plot
usePattern_plot = surf(-plot_radius:plot_radius, -plot_radius:plot_radius, usePattern);

% Get rid of edges
usePattern_plot.EdgeColor = 'none';

% Make color brighter
shading flat;

% Uncomment below if you need a color bar
%colorbar;

% titles and labels
title('Space Use Pattern');
xlabel('x-position');
ylabel('y-position');