close all;

area = 100;

P = 3; %??
k = 0.3;
S = 0.5 / sqrt(P);
sd = 1.92 / (k * S^2);

result = zeros(2*area+1, 2*area+1);


for x = -area:area
    for y=-area:area
        D = sqrt(x*x + y*y);
        result(x+area+1, y+area+1) = 3*exp(-sqrt(3)*D/sd)/(2*pi*sd^2);
        fprintf("Result for x:%d, y:%d: %d\n", x, y, result(x+area+1, y+area+1));
    end
end

hold on;
surf([-area:area], [-area:area], result);