% based on Olfactory Orientation Model
 
 
close all;
 
T = 1000; % the max step of simulation
N = 5;
k = 0.3;
S_b = 0.5;
P = 1;
 
sigma = zeros(N,T);
alpha = zeros(N,T);
theta = zeros(N,T+1);
X = zeros(N,T+1);
Y = zeros(N,T+1);
D = zeros(N,T+1);
dD = zeros(N,T+1);
 
sigma_b = S_b * sqrt(P);
 
delta = 0.8/k;
 
R = 2.74 * delta;
 
for i = 1:N
    theta(i,1) = rand() * 2 * pi; % start with a random orientation
    X(i,1) = rand() * 50 - 25;
    Y(i,1) = rand() * 50 - 25;
end
 
for ind = 1:N
    for i = 1:T
        dislike = [0,0];
        for j = 1:T-1
            for ind2 = 1:N
               if ind2 ~= ind
                    dis = ((X(ind,i)-X(ind2,j))^2 + (Y(ind,i)-Y(ind2,j))^2)^0.5;
                    if dis <= R
                        dislike(1) = dislike(1) + 1 /(X(ind,i)-X(ind2,j));
                        dislike(2) = dislike(2) + 1/ (Y(ind,i)-Y(ind2,j));
                    end
               end
            end
        end
 
        theta_dislike = inf;
        if dislike(1) + dislike(2) > 0
            theta_dislike = atan(dislike(2)/dislike(1));
        end
 
        sigma(ind,i) = sigma_b * (1 + k * dD(ind,i) / P);
        alpha(ind,i) = normrnd(0,sigma(ind,i));
        theta(ind,i+1) = theta(ind,i) + alpha(ind,i);
 
        if theta_dislike ~= inf
            a = min(theta(ind,i+1), theta_dislike);
            b = max(theta(ind,i+1), theta_dislike);
            theta(ind,i+1) = rand() * mod(b-a, 2 * pi) - a;
        end
 
        X(ind,i+1) = X(ind,i) + P * cos(theta(ind,i+1));
        Y(ind,i+1) = Y(ind,i) + P * sin(theta(ind,i+1));
        D(ind,i+1) = ((X(ind,i+1)-X(ind,1))^2 + (Y(ind,i+1)-Y(ind,1))^2)^0.5;
        dD(ind,i+1) = D(ind,i+1) - D(ind,i);
    end
end
ax= gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
get(gca,'ColorOrder')
figure(1);
hold on;  
for i = 1:N
    plot(X(i,:),Y(i,:)) % Figure 1
end