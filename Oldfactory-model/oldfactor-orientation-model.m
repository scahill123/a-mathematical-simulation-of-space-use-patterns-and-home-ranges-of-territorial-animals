close all;

N = 1000; % the max step of simulation
k = 0.3;
S_b = 0.5;
P = 1;

sigma = zeros(1,N);
alpha = zeros(1,N);
theta = zeros(1,N+1);
X = zeros(1,N+1);
Y = zeros(1,N+1);
D = zeros(1,N+1);
dD = zeros(1,N+1);

sigma_b = S_b * sqrt(P);
theta(1) = rand() * 2 * pi; % start with a random orientation

for i = 1:N
    sigma(i) = sigma_b * (1 + k * dD(i) / P);
    alpha(i) = normrnd(0,sigma(i));
    theta(i+1) = theta(i) + alpha(i);
    X(i+1) = X(i) + P * cos(theta(i+1));
    Y(i+1) = Y(i) + P * sin(theta(i+1));
    D(i+1) = ((X(i+1)-X(1))^2 + (Y(i+1)-Y(1))^2)^0.5;
    dD(i+1) = D(i+1) - D(i);
end

figure(1);
plot(X,Y) % Figure 1






N = 10000; % the max step of simulation
k = 0.3;
S_b = 0.5;
P = 1;

sigma = zeros(1,N);
alpha = zeros(1,N);
theta = zeros(1,N+1);
X = zeros(1,N+1);
Y = zeros(1,N+1);
D = zeros(1,N+1);
dD = zeros(1,N+1);

sigma_b = S_b * sqrt(P);
theta(1) = rand() * 2 * pi; % start with a random orientation

delta = 1.92/(k*sigma_b);

f =  zeros(1000,1000);
for i = 1:N
    sigma(i) = sigma_b * (1 + k * dD(i) / P);
    alpha(i) = normrnd(0,sigma(i));
    theta(i+1) = theta(i) + alpha(i);
    X(i+1) = X(i) + P * cos(theta(i+1));
    Y(i+1) = Y(i) + P * sin(theta(i+1));
    D(i+1) = ((X(i+1)-X(1))^2 + (Y(i+1)-Y(1))^2)^0.5;
    dD(i+1) = D(i+1) - D(i);
    
    f(i+1) = 3 * exp((-sqrt(3)*D(i+1)/delta)/(2*pi*delta^2));
end

figure(2);
scatter3(X(2:N+1),Y(2:N+1),f(2:N+1))% Figure 2
