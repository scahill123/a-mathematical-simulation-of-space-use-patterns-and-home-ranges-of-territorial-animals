close all;

%% Constants for Plotting
map_size_x = 250;
map_size_y = 200;
num_nesting_sites = 50;

% Any number that is a coprime of num_nesting_sites
% and smaller than num_nesting_sites
rand_coprime = 7;

% Number of animals to TRY locating in the area.
% Note: Actual animal in the maps will be LESS than this!
num_locating = 100;

%% Nesting Sites
% Randomly distributing the nesting sites
nesting_sites_x = rand(1, num_nesting_sites) .* map_size_x;
nesting_sites_y = rand(1, num_nesting_sites) .* map_size_y;

% Plot the distributed nesting sites
scatter(nesting_sites_x, nesting_sites_y, 'r^');
hold on;

occupation = zeros(1, num_nesting_sites);
home = zeros(1, num_nesting_sites);
range_sizes = zeros(1, num_locating);

S_b = zeros(1, num_locating);
delta = zeros(1, num_locating);
k = zeros(1, num_locating);

%% For each Animals
for i = 1:num_locating
    % Decide an animal
    % Animal-specific constants
    P = 1; % P: step size
    S_b(i) = 0.5 / sqrt(P); % Basic Sinuosity: sigma / sqrt(P)
    k(i) = rand(1); % klinokinetic factor (0 < k = a*c < 1).
    
    % Variable dependent to constants
    delta(i) = 1.92 / (k(i) * S_b(i)^2); % Standard deviation of ...
                                    % the utilization distribution
    
    % Choose an Unoccupied Nesting Site (home)
    % And save to 'new occupation'
    new_occupation = get_new_occupation(num_nesting_sites, ...
        occupation, rand_coprime);
    if new_occupation == -1
        % Boundary case: all nesting sites are occupied, 
        % no need to locate more.
        break;
    end

    % Calculate size of home range 
    range_radius = 2.74 * delta(i);
    range_radius = range_radius * 0.7; % Custom factor
    range_sizes(i) = range_radius;
    
    % Occupation
    survive = true;
    for j = 1:num_nesting_sites
        if sqrt((nesting_sites_x(new_occupation)-nesting_sites_x(j))^2 ...
                + (nesting_sites_y(new_occupation)-nesting_sites_y(j))^2) ...
                < range_radius
            
            % Check if the close nest is occupied
            % 1. It's occupied!
            if (occupation(j) ~= 0)
                % If conflict occurs, randomly decide who will survive.
                % 20% new one survive, otherwise the previous one survive
                
                % Previous one dies
                if (rand(1) < 0.2) 
                    % occupation(j) is killed by i
                    j_num = occupation(j);
                    for j_nest = 1:num_nesting_sites
                        if occupation(j_nest) == j_num
                            occupation(j_nest) = 0;
                            home(j_nest) = 0;
                        end
                    end
                    
                % Current one dies
                else
                    % i is dead
                    survive = false;
                    for new_nest = 1:num_nesting_sites
                        if occupation(new_nest) == i
                            occupation(new_nest) = 0;
                        end
                    end
                    if (~survive) 
                        break;
                    end
                end
                
            % 2. It's not occupied
            else
                occupation(j) = i;
            end
        end
    end
    
    if (survive)
        % i survived
        home(new_occupation) = i;
    end
    
end

%%
% For each animal, draw the home range circle.
for i = 1:num_nesting_sites
    if (home(i) ~= 0)
        fprintf("Plotting for nesting site #%d (%d, %d)\n", i, ...
            nesting_sites_x(i), nesting_sites_x(i));
        animal = home(i);
        % draw a circle of size: range_sizes(animal)
%         circle(nesting_sites_x(i), nesting_sites_y(i), ...
%             range_sizes(animal));
    end
end

%%
% For each animal, run the Benhamou walk.
for j = 1:num_nesting_sites
    if (home(j) ~= 0)
        fprintf("Plotting for nesting site #%d (%d, %d)\n", j, ...
            nesting_sites_x(j), nesting_sites_x(j));
        animal = home(j);
        
        % Initial step
        P = 1;
        t_max = 500;
        
        % set the x, y as (0, 0)
        x_pos = nesting_sites_x(j);
        y_pos = nesting_sites_y(j);
        
        % containers
        x_result = zeros(1, t_max);
        y_result = zeros(1, t_max);
        x_result(1) = x_pos;
        y_result(1) = y_pos;
        
        % Decide a random orientation theta
        theta = rand(1) * pi;
        
        % set the distance
        D = 0;
        dD = 0;
        
        % For each step,
        for i = 2:t_max
            % Decide sigma_b and sigma(i) from previous distance
            sigma_b = S_b(animal) * sqrt(P);
            sigma = sigma_b * (1 + k(animal) * dD / P);
            
            % Decide alpha, following the normal distribution
            alpha = normrnd(0, sigma);
            
            % Decide theta
            prev_theta = theta;
            theta = prev_theta + alpha;
            
            % Decide the (x, y) position
            prev_x_pos = x_pos;
            prev_y_pos = y_pos;
            x_pos = prev_x_pos + P * cos(theta);
            y_pos = prev_y_pos + P * sin(theta);
            
            % Update the distance
            prev_D = D;
            D = sqrt((x_pos-x_result(1))^2 + (y_pos-y_result(1))^2);
            dD = D - prev_D;
        
            % Save the result
            x_result(i) = x_pos;
            y_result(i) = y_pos;
        end
        % plot walk
        plot(x_result, y_result);
        % plot convex hull
        [points,av] = convhull(x_result(:), y_result(:));
        plot(x_result(points), y_result(points), 'linewidth', 1, 'color', '#AAA');
    end
end



%%
% returns a random occupation
function new_occupation = get_new_occupation(num_nesting_sites, ...
        occupation, rand_coprime)
    new_occupation = -1;
    rand_pick = ceil(rand(1) * num_nesting_sites);
    j = rand_pick;
    started = false;
    while (~started) || (j ~= rand_pick)
        started = true;
        % Check if the site is occupied
        if occupation(j) == 0
            new_occupation = j;
            break;
        end
        j = rem(j + rand_coprime, num_nesting_sites);
        if j == 0
            j = num_nesting_sites;
        end
    end
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit);
end