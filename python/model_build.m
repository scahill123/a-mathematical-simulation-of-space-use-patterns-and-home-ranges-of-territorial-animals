M = load('mod_wolf_matlab.txt');
helper1 = unique(M(:,3));
data_set=1;
%[1 2 4]
%Latham ADM (2009) Wolf ecology and caribou-primary prey-wolf spatial relationships in low productivity peatland complexes in northeastern Alberta. Dissertation. ProQuest Dissertations Publishing, University of Alberta, Canada, NR55419, 197 p. url:http://search.proquest.com/docview/305051214
%Latham ADM, Boutin S (2019) Data from: Wolf ecology and caribou-primary prey-wolf spatial relationships in low productivity peatland complexes in northeastern Alberta. Movebank Data Repository. doi:10.5441/001/1.7vr1k987
lon=mean(M(:,1))*pi/180;
%use long and lat of edmonton as origin
lat=mean(M(:,2))*pi/180;
a=6378137.0000;
b=6356752.3142;
e=sqrt(1-(b/a)^2);
ee=sqrt((a/b)^2-1);
K=(((a^2*cos(lat))/b)/sqrt(1+(ee)^2*(cos(lat))^2));
for i = data_set
    h1=helper1(i);
    helper2=M(M(:,3)==h1,1);
    helper3=M(M(:,3)==h1,2);
    helper4=M(M(:,3)==h1,4);
    h4=size(helper2);
    M_xy = zeros(h4(1),2);
    for j =1:h4
    M_xy(j,1)=K*(helper2(j)-lon);
    M_xy(j,2)=K*log(tan(pi/4+helper3(j)/2)*((1-e*sin(helper3(j)))/(1+e*sin(helper3(j))))^(e/2));
    M_xy(j,3)=helper4(j);
    end
%     plot(real(M_xy(:,1)),real(M_xy(:,2)));
%     hold on
end
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
speed=zeros(size(M_xy,1),1);
freq_dis=zeros(size(M_xy,1),2);
abs_dis = zeros(size(M_xy,1),1);
for i=2:size(M_xy,1)
    dis=(sqrt((M_xy(i,1)-M_xy(1,1))^2+(M_xy(i,2)-M_xy(1,2))^2))/1000;
    speed(i,1)=dis/sum(M_xy(1:i,3));
    hx=speed(i,1);
    freq_dis(i,1)=hx;
    freq_dis(i,2)=freq(hx,-0.5,0.5);
   % hxx=M_xy(i,2)-M_xy(i-1,2)/M_xy(i,1)-M_xy(i-1,1);
    angle = atan2(M_xy(i,2)-M_xy(i-1,2),M_xy(i,1)-M_xy(i-1,1));
%     if(hxx<0)
%         disp(1)
%     end
    if angle>0
        true_dis=(sqrt((M_xy(i,1)-M_xy(1,1))^2+(M_xy(i,2)-M_xy(1,2))^2))/1000;
    else
        true_dis=-((sqrt((M_xy(i,1)-M_xy(1,1))^2+(M_xy(i,2)-M_xy(1,2))^2))/1000);
    end
    abs_dis(i,1)=true_dis;
end
[f,xi]=ksdensity(speed);
subplot(221)
plot(xi,f);
subplot(222)
plot(speed);
% subplot(223);
% plot(freq_dis(:,1),freq_dis(:,2));
% subplot(224);
% [f2,x2]=ksdensity(freq_dis);
% plot(x2,f2);
%set init at first location
subplot(223)
plot(1:2301,abs_dis(:,1));
save('fit_by_python.txt','abs_dis','-ascii');
