file='wolf.txt';
T=readtable(file);
date_time = zeros(15159,1);
mult_data=zeros(15159,3);
date_data = zeros(15159,16);
mult_data(1,1:3) =double(table2array(T(1,1:3))) ;
result_data=zeros(15159,4);
for j=2:15159
mult_data(j,1:3) =double(table2array(T(j,1:3))) ;
x=char(table2array(T(j-1,4)));
y = datestr(x,'yyyy-mm-dd HH:MM');
x2 = char(table2array(T(j,4)));
y2 = datestr(x2,'yyyy-mm-dd HH:MM');
y3=datevec(y2)-datevec(y);
date_time(j,1) = fix(etime(datevec(y2),datevec(y))/3600);
end
for i=2:15159
    if date_time(i,1)==0
        date_time(i,1)=30;
    else
        date_time(i,1)=date_time(i,1)*60;
    end
end
for i=1:15159
    result_data(i,1:3)=mult_data(i,1:3);
    result_data(i,4)=date_time(i,1);
end
save('mod_wolf_matlab.txt','result_data','-ascii');