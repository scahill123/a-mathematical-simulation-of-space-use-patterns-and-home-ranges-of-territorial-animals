import json
import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
import collections
import math
import random
from scipy.optimize import curve_fit
def func(t,A,L):
	h2=A*np.sqrt(np.abs(t)*L)
	return h2
def func1(t,A,L):
	h2=A*np.sqrt(np.abs(t)*L)
	return -(h2)
def func3(t,A,L):
	h1=np.zeros(2300)
	for i in range(2300):
		random.seed(a=None,version=2)
		h1[i]=random.random()
	h2=1/2*(1-(t/10))
	return t+A*((h2-h1)/abs(h2-h1))

if __name__ =="__main__":
	df = pd.read_csv('fit_by_python.txt',names=['location'],encoding="unicode_escape")
	array_from_df = df.to_numpy()
	true_array=np.reshape(array_from_df,(-1))
	input_array=np.delete(true_array,-1)
	output_array=np.delete(true_array,0)
	hi=np.reshape(input_array,(-1))
	ho=np.reshape(output_array,(-1))
	x_axis = range(2300)
	hx=np.zeros(1150)
	hy=np.zeros(1150)
	for i in range(1,2300):
		if (i%2 != 0):
			hx[((i-1)//2)]=hi[i-1]
		else:
			hy[((i-1)//2)]=ho[i-1]
	arr_T = true_array>0
	lh1=list()
	lh2=list()
	lh3=list()
	lh4=list()
	for i in range(1,2301):
		if(arr_T[i]):
			lh1.append(true_array[i-1])
			lh2.append(true_array[i])
		else:
			lh3.append(true_array[i-1])
			lh4.append(true_array[i])
	plot1 = plt.plot(lh1,lh2,'b.',markersize=2,label='positive')
	plot2 = plt.plot(lh3,lh4,'b.',markersize=2,label='negative')
	Alh1=np.array(lh1)
	Alh2=np.array(lh2)
	Alh3=np.array(lh3)
	Alh4=np.array(lh4)
	intercept,poly_cov = curve_fit(func,Alh1,Alh2)
	intercept1,poly_cov1 = curve_fit(func1,Alh3,Alh4)
	print(Alh1.shape)
	print(Alh2.shape)
	print(Alh3.shape)
	print(Alh4.shape)
	fit_model = func(lh1,intercept[0],intercept[1])
	fit_model2 = func1(lh3,intercept1[0],intercept1[1])
	last_today=fit_model2
	counter=0
	for i in range(1,2301):
		if(arr_T[i]):
			last_today=np.insert(last_today,i,fit_model[counter])
			counter=counter+1
	res_pts = np.append(fit_model,fit_model2)
	plot2 = plt.plot(Alh1,fit_model,'r+',label='fit_model_based_on_positive')
	plot3 = plt.plot(Alh3,fit_model2,'r+',label='fit_model_based_on_negative')
	plt.xlabel('previous_location')
	plt.ylabel('next')
	plt.legend(loc=1)
	plt.title('origin vs fit')
	print(intercept[0])
	print(intercept[1])
	print(intercept1[0])
	print(intercept1[1])
	plt.show()
