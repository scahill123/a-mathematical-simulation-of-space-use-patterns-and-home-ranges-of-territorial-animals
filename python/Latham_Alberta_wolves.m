M = load('mod_wolf_matlab.txt');
helper1 = unique(M(:,3));
data_set=[1 2 4];
%Latham ADM (2009) Wolf ecology and caribou-primary prey-wolf spatial relationships in low productivity peatland complexes in northeastern Alberta. Dissertation. ProQuest Dissertations Publishing, University of Alberta, Canada, NR55419, 197 p. url:http://search.proquest.com/docview/305051214
%Latham ADM, Boutin S (2019) Data from: Wolf ecology and caribou-primary prey-wolf spatial relationships in low productivity peatland complexes in northeastern Alberta. Movebank Data Repository. doi:10.5441/001/1.7vr1k987
lon=mean(M(:,1))*pi/180;
%use long and lat of edmonton as origin
lat=mean(M(:,2))*pi/180;
a=6378137.0000;
b=6356752.3142;
e=sqrt(1-(b/a)^2);
ee=sqrt((a/b)^2-1);
K=(((a^2*cos(lat))/b)/sqrt(1+(ee)^2*(cos(lat))^2));
for i = data_set
    h1=helper1(i);
    helper2=M(M(:,3)==h1,1);
    helper3=M(M(:,3)==h1,2);
    h4=size(helper2);
    M_xy = zeros(h4(1),2);
    for j =1:h4
    M_xy(j,1)=K*(helper2(j)-lon);
    M_xy(j,2)=K*log(tan(pi/4+helper3(j)/2)*((1-e*sin(helper3(j)))/(1+e*sin(helper3(j))))^(e/2));
    end
    plot(real(M_xy(:,1)),real(M_xy(:,2)));
    hold on
end
legend('adult,Male','2-years-old, Female','adult Female');
xlabel('horizontal direction unit:M');
ylabel('vertical direction unit:M');
title('visual movement of wolves from 2006-02-06 to 2007-08-30');
hold off