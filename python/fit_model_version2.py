import json
import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import collections
import math
import random
from scipy.optimize import curve_fit
def func(t,A,L):
	h2=A*np.sqrt(np.abs(t)*L)
	return h2
def func1(t,A,L):
	h2=A*np.sqrt(np.abs(t)*L)
	return -(h2)
def func3(t,A,L):
	h1=np.zeros(2300)
	for i in range(2300):
		random.seed(a=None,version=2)
		h1[i]=random.random()
	h2=1/2*(1-(t/10))
	return t+A*((h2-h1)/abs(h2-h1))

font = {'family' : 'normal',
        'size'   : 20}

matplotlib.rc('font', **font)

if __name__ =="__main__":
	df = pd.read_csv('fit_by_python.txt',names=['location'],encoding="unicode_escape")
	array_from_df = df.to_numpy()
	true_array=np.reshape(array_from_df,(-1))
	input_array=np.delete(true_array,-1)
	output_array=np.delete(true_array,0)
	hi=np.reshape(input_array,(-1))
	ho=np.reshape(output_array,(-1))
	x_axis = range(2300)
	hx=np.zeros(1150)
	hy=np.zeros(1150)
	for i in range(1,2300):
		if (i%2 != 0):
			# print(i-1)
			hx[((i-1)//2)]=hi[i-1]
		else:
			# print(i-1)
			hy[((i-1)//2)]=ho[i-1]
	arr_T = true_array>0
	lh1=list()
	lh2=list()
	lh3=list()
	lh4=list()
	for i in range(1,2301):
		if(arr_T[i]):
			lh1.append(true_array[i-1])
			lh2.append(true_array[i])
		else:
			lh3.append(true_array[i-1])
			lh4.append(true_array[i])
	plot1 = plt.plot(lh1,lh2,'r.',markersize=2,label='positive')
	plot2 = plt.plot(lh3,lh4,'b.',markersize=2,label='negative')
	plt.title('Distance between previous and next location')
	plt.xlabel('Previous location')
	plt.ylabel('Next location')
	plt.legend(loc=1)
	plt.savefig('top-bottom.png', format='png')
	plt.show()
