# import spacy
import json
import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
# from wordcloud import WordCloud
import collections
# from sklearn.linear_model import LinearRegression
# import nltk
import math
import random
from scipy.optimize import curve_fit
def func(t,A,L):
	# h1=np.zeros(1150)
	# for i in range(1150):
	# 	random.seed(a=None,version=2)
	# 	h1[i]=random.random()
	h2=A*np.sqrt(np.abs(t))
	# if (t==0 and h1<=1/2):
	# 	return A
	# if ((t==0) and h1>1/2):
	# 	return -A
	# if((h1<h2)):
	# 	res=(t+A)
	# else:
	# 	res=(t-A)
	# t+A*((h2-h1)/abs(h2-h1))
	return L+h2

if __name__ =="__main__":
	df = pd.read_csv('fit_by_python.txt',names=['location'],encoding="unicode_escape")
	print(df.head())
	array_from_df = df.to_numpy()
	print(type(array_from_df))
	true_array=np.reshape(array_from_df,(1,2301))
	input_array=np.delete(true_array,-1,axis=1)
	output_array=np.delete(true_array,0,axis=1)
	hi=np.reshape(input_array,(-1))
	ho=np.reshape(output_array,(-1))
	x_axis = range(2300)
	hx=np.zeros(1150)
	hy=np.zeros(1150)
	for i in range(1,2301):
		if (i%2 != 0):
			hx[((i-1)//2)]=hi[((i-1)//2)]
		else:
			hy[((i-1)//2)]=ho[((i-1)//2)]

	print(hx.shape)
	print(hy.shape)
	intercept,poly_cov = curve_fit(func,hx,np.abs(hy))
	# print(np.max(ho))
	fit_model = func(hx,intercept[0],intercept[1])
	plot1 = plt.plot(hx,np.abs(hy),'b+',label='Positive origin data')
	plot2 = plt.plot(hx,fit_model,'r+',label='Fit data from model')
	plt.xlabel('Current Location')
	plt.ylabel('Next Location')
	plt.legend(loc=1)
	plt.title('Positive origin data vs Fit data')
	plt.savefig('model.png', format='png')
	plt.show()
	print(intercept[0])
	print(intercept[1])
