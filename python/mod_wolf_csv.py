# import spacy
import json
import numpy as np
import re
import pandas as pd
import matplotlib.pyplot as plt
# from wordcloud import WordCloud
import collections
# from sklearn.linear_model import LinearRegression
# import nltk
import math
import codecs

def build_df(fileName):
	# columns = ['id','text']
	df = pd.read_csv(fileName,encoding="unicode_escape")
	# df['tidy_tweet']=df['text'].apply(remove_url_punctuation)
	# df['en'] = df['text'].apply(detect_lang)
	# df = df[df['en']=='en']
	# df = df.head(1000)
	# df['word_list']=df['tidy_tweet'].apply(split_words)
	# df['nlp_list']=df['word_list'].apply(remove_stopwords)
	return df

if __name__ =="__main__":
	#build dataframe and get tokens, get words with/without stopwords
	f=codecs.open("wolf.txt","w",encoding='utf8')
	df1 = build_df('Latham Alberta Wolves.csv')
	# print(df1.head())
	# print(df1.columns)
	# print(df1[['timestamp','location-long','location-lat']])
	# print(df1.size)
	# print(type(df1['timestamp'][1]))
	for i in range(df1['timestamp'].size):
		with codecs.open("wolf.txt","a+",encoding='utf8') as f:
			f.write(str(df1['location-long'][i])+","+str(df1['location-lat'][i])+","+str(df1['individual-local-identifier'][i])+","+str(df1['timestamp'][i])+"\n")
