% Finite Difference Method

close all;

step = 0.1;
range = [-100, 100];

max_n = ceil((range(2) - range(1)) / 0.1);
X = zeros(1,  max_n);

D = 1;

P = zeros(1,  max_n);

for t = 1:10:500
    for i = 1:max_n
    x = i * 0.1 -  (range(2) - range(1)) / 2;
    P(i) = 1.0/(4*pi*D*t)^0.5 * exp( -x^2 /(4 * D *t));
    end
    plot( range(1):step:range(2)-step, P )
    title("t=" + t);
    pause(0.1)
end
