% Finite Difference Method
 
close all;

% Constants for plotting
step = 0.1;
range = [-100, 100];
max_n = ceil((range(2)-range(1))/0.1);

% Constants for the model
D = 1;
t = 50;

% Computing the PDE
P = zeros(1,  max_n);
for i = 1:max_n
    x = i * 0.1 - (range(2) - range(1))/2;
    P(i) = 1.0/(4*pi*D*t)^0.5 * exp( -x^2 /(4 * D *t));
end


plot( range(1):step:range(2)-step, P );
title("t = " + t);
