% Biased random walk generator
% Based on: Diffusion and Ecological Problems Modern Perspective
% Akira Okubo and Louis Gross, Page 240. (Section 8.2)

% ===============================================================
% Modify the below to change the constants

% Contants for model
ep = 0.5; % epsilon: constant, ep < 1.

% Constants for plotting
t_max = 500;

% ===============================================================
% Logic starts here

t = 1:t_max;
x = zeros(1, t_max);

x(1) = 0;
for i = 2:t_max
    prev_x = x(i-1);
    rand_val = rand(1);
    if (prev_x == 0)
        % (This part is here to prevent division by zero)
        % walking to either prev_x+1 or prev_x-1 with equal probability
        if (rand_val < (1 / 2)) 
            x(i) = prev_x + 1;
        else
            x(i) = prev_x - 1;
        end
        
    elseif (rand_val < (1 / 2) * (1 - ep/prev_x))
        % walking to prev_x+1 with probability (1/2)*(1-ep/prev_x);
        x(i) = prev_x + 1;
        
    else
        % walking to prev_x-1 with probability (1/2)*(1+ep/prev_x);
        x(i) = prev_x - 1;
        
    end
end

% plot the random walk with blue
plot(t, x, 'Color', 'blue', 'LineWidth', 1);
title('Diminishing-Central-Bias Random Walk Simulation')
xlabel('Time Steps') 
ylabel('Displacement') 

% plot the line x=0 with red
hold on;
plot(t, zeros(1, t_max), 'Color', 'red', 'LineWidth', 1);