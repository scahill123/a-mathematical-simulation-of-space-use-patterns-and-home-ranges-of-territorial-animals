close all;

% Biased random walk.
% Based on: Diffusion and Ecological Problems Modern Perspective
% Akira Okubo and Louis Gross, Page 240. (Section 8.2)

% ===============================================================
% Modify the below to change the constants

% Contants for model
L = 20; % 2L is the home range size, |x| is bounded by L

% Constants for plotting
t_max = 3000;

% ===============================================================
% Logic starts here

t = 1:t_max;
x = zeros(1, t_max);

% Calculating total excursions
total_excursion = 0;

x(1) = 0;
for i = 2:t_max
    prev_x = x(i-1);
    rand_val = rand(1);
    
    if (rand_val < (1 / 2) * (1 - prev_x/L))
        % walking to prev_x+1 with probability (1/2)*(1-prev_x/L);
        x(i) = prev_x + 1;
        
    else
        % walking to prev_x-1 with probability (1/2)*(1+prev_x/L);
        x(i) = prev_x - 1;
        
    end
    
    total_excursion = total_excursion + abs(x(i));
end

% plot the random walk with blue
plot(t, x, 'Color', 'blue', 'LineWidth', 1);
title('Centrally-biased Random Walk Simulation')
xlabel('Time Steps') 
ylabel('Displacement') 

% plot the line x=0 with red
hold on;
plot(t, zeros(1, t_max), 'Color', 'red', 'LineWidth', 1);

eae = "Expected average excursion: " + num2str(sqrt(pi*L))
mae = "Measured average excursion: " + num2str(total_excursion/t_max)
% Evaluate the expected and actual avg excursion:
text(t_max*0.1,min(x)*0.95,eae)
text(t_max*0.6,min(x)*0.95,mae)
fprintf("Expected average excursion: sqrt(pi*L) = %d\n", sqrt(pi*L));
fprintf("Measured average excursion: %d\n", total_excursion/t_max);