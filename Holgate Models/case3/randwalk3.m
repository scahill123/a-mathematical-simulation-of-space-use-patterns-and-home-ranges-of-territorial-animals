close all;

% Biased random walk.
% Based on: Diffusion and Ecological Problems Modern Perspective
% Akira Okubo and Louis Gross, Page 240. (Section 8.2)

% ===============================================================
% Modify the below to change the constants

% Constants for plotting
days = 2;
cycles = 1;

% ===============================================================
% Logic starts here
t_max = 2400*days
t = 1:t_max;
x = zeros(1, t_max);

x(1) = 0;
for i = 2:t_max
    prev_x = x(i-1);
    rand_val = 2*rand(1)-1;
    
    if (rand_val < -cos(2*cycles*pi/2400*i))
        x(i) = prev_x + 1;
    
    elseif prev_x>0
            x(i) = prev_x-1;
    else 
        x(i)=prev_x;
    end
    
end

% plot the random walk with blue
plot(t, x, 'Color', 'blue', 'LineWidth', 1);
title('Daylight-Governed Random Walk Simulation')
xlabel('Time Steps') 
ylabel('Discplacement') 

% plot the line x=0 with red
hold on;
plot(t, zeros(1, t_max), 'Color', 'red', 'LineWidth', 1);