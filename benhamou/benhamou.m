x_limit=10000; % number of steps
k=0.3; 
sb=0.5; % sinousity
p = 1; % length of steps
ob = sb*sqrt(p); % std

% initialize 7-by-x_limit matrix for 7 parameters that characterize every
% step
result_set=zeros(7,x_limit); 

% include x,y,oi,ai,0i,d,dD
% set home as (1,1)
result_set(1,1) = 1;
result_set(2,1) = 1;
% randomly set initial orientation
result_set(5,1) = deg2rad(rand()*360);
result_set(3,1) = ob;

for i=2:x_limit
    result_set(3,i)=ob*(1+k*result_set(7,i-1)/p); % standard deviation (std)
    result_set(4,i-1) = normrnd(0,result_set(3,i-1)); % draw from N(0,std)
    result_set(5,i) = result_set(5,i-1)+result_set(4,i-1); % new orientation 
    result_set(1,i) = result_set(1,i-1)+p*cos(result_set(5,i)); % new X
    result_set(2,i) = result_set(2,i-1)+p*sin(result_set(5,i)); % new Y
    result_set(6,i) = sqrt((result_set(1,i)-result_set(1,1))^2+(result_set(2,i)-result_set(2,1))^2); % new distance from home (1,1)
    result_set(7,i) = result_set(6,i)-result_set(6,i-1); % change in distance from last step
end

% plot the random walk
plot(result_set(1,1:x_limit),result_set(2,1:x_limit)); % plot all (X,Y) formulated earlier
title('1000 step Old Factory Orientation Model')
xlabel('X') 
ylabel('Y') 