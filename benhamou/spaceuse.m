close all;
x=-5000:5:5000;
y=-5000:5:5000;
p=5;
k=0.5;
tre=0.3;
Sb=0.3/sqrt(p);
si = 1.92/(k*Sb^2);
[xx,yy]=meshgrid(x,y);
D = sqrt(xx.^2+yy.^2);
res = 3*exp(-sqrt(3).*D/si)/si^2;
surf(xx,yy,res);
shading flat
